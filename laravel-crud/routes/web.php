<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::get('/pertanyaan/{pertanyaan}', 'PertanyaanController@show');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::delete('/pertanyaan/{pertanyaan}', 'PertanyaanController@destroy');
Route::get('/pertanyaan/{pertanyaan}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan}', 'PertanyaanController@update');

// Route::resource('pertanyaan', 'PertanyaanController');