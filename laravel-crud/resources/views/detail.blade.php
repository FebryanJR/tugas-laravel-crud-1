@extends('templates.main')

@section('title', 'Detail')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-6">
			<h1 class="my-3">DETAIL PERTANYAAN</h1>

	<div class="card mt-2 ml-1">
	  <div class="card-body">
		    <h5 class="card-title">{{ $pertanyaan->judul }}</h5>
		    <p class="card-text">{{ $pertanyaan->isi }}</p>
		    
		    <a href="{{$pertanyaan->id}}/edit" class="btn btn-primary">Edit</a>

		    <form action="/pertanyaan/{{$pertanyaan->id}}" method="post" class="d-inline">
		    	@method('delete')
		    	@csrf
		    <button type="submit" class="btn btn-danger">Delete</button>
		    </form>

		    <a href="/pertanyaan" class="card-link ml-2">Back</a>
	  </div>
</div>
@endsection