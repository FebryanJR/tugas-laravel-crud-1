@extends('templates.main')

@section('title', 'Pertanyaan')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-6">
			<a href="/pertanyaan/create" class="btn btn-primary my-3">BUAT PERTANYAAN</a>

			@if (session('status'))
			    <div class="alert alert-success">
			        {{ session('status') }}
			    </div>
			@endif

			<h1 class="my-3">DAFTAR PERTANYAAN</h1>

			<!-- <ul class="list-group">
				@foreach ($pertanyaan as $quest)
				  <li class="list-group-item d-flex justify-content-between align-items-center">
				 	{{$quest->judul}}
				    <a href="/pertanyaan/{{$quest->id}}" class="badge badge-primary">Detail</a>
				  </li>
				 @endforeach
			</ul> -->

			<table class="table table-bordered">
			  <thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Judul</th>
			      <th scope="col">Isi</th>
			      <th scope="col">Aksi</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $i =1; ?>
			  	@foreach($pertanyaan as $quest)
			    <tr>
			      <th scope="row">{{$i}}</th>
			      <td>{{$quest->judul}}</td>
			      <td>{{$quest->isi}}</td>
			      <td><a href="/pertanyaan/{{$quest->id}}" class="badge badge-primary">Detail</a></td>
			    </tr>
			    <?php $i++; ?>
			    @endforeach
			  </tbody>
			</table>
		</div>
	</div>
</div>
@endsection