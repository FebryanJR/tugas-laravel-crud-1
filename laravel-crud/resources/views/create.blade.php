@extends('templates.main')

@section('title', 'Create')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-6">
			<h1 class="mt-3">FORM BUAT PERTANYAAN</h1>
				<form action="/pertanyaan" method="post">
					@csrf

				   <div class="form-group">
				    <label for="judul">Judul</label>
				    <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" value="{{old('judul')}}">
				 	  @error('judul')
				 	  <div class="invalid-feedback">
				        {{ $message }}
				      </div>
				      @enderror
				  </div>

				  <div class="form-group">
				    <label for="isi">Isi</label>
    				<textarea class="form-control @error('isi') is-invalid @enderror" id="isi" rows="3" name="isi">
    				{{old('isi')}}</textarea>
	    			  @error('isi')
				 	  <div class="invalid-feedback">
				        {{ $message }}
				      </div>
				      @enderror
				  </div>

				  <button type="submit" class="btn btn-primary">Tambah Pertanyaan!</button>
				</form>
		</div>
	</div>
</div>
@endsection